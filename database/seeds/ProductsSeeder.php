<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{

    use \App\Traits\SettingsDatabaseTrait;

    public function run()
    {

        /**
         * #simp - it is better to create migration and model for sst__product_types
         * #simp - also I do not check for duplicates while seeding
         */

        $table = 'products';
        $qty_to_seed = 20;

        /** How much products do you want? **/
        $type = $this->sdt__product_types[0];//service
        foreach (range(1, $qty_to_seed) as $value){
            $n = Str::random(5);
            DB::table($table)->insert([
                'title' => 'Ремонт холодильника марки '.$n,
                'price_cents' => mt_rand(20000,50000),
                'type' => $type
            ]);
        }


    }
}
