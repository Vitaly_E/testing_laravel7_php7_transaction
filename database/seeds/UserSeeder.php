<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** How much users you want? **/
        foreach (range(1,5) as $value){
            $n = \Illuminate\Support\Str::random(5);
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => $n,
                'email' => $n.'@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('111111'),
            ]);
        }
    }
}
