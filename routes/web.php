<?php

use Illuminate\Support\Facades\Route;



Route::get('/', 'HomeController@main')->name('main');

Route::post('/pay', 'Payments\PayController@pay')->name('pay');
Route::post('/checkout', 'Payments\PayController@checkout')->name('checkout');

Route::get('/pay', 'HomeController@toMain')->name('pay_not_allowed');
Route::get('/checkout', 'HomeController@toMain')->name('checkout_not_allowed');

