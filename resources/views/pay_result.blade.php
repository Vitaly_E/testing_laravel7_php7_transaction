@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Результат оплаты</div>

                    <div class="card-body">
                        <h5>{{$result}}</h5>
                        <br>
                        <a href="/" class="btn btn-secondary btn-sm">Домой</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
