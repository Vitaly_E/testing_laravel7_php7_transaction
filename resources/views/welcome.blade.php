@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('payments.enter_client_fio') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('pay')}}">
                            @csrf

                            <input type="hidden" value="{{$products->id}}" name="product_id">

                            <div class="form-group row">
                                <label for="client"
                                       class="col-md-4 col-form-label text-md-right">Фамилия Имя Отчество:</label>

                                <div class="col-md-6">
                                    <input id="client" type="text"
                                           class="form-control @error('client') is-invalid @enderror"
                                           name="client" value="{{$client}}"
                                           required autocomplete="client" autofocus>
                                    @error('client')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="product"
                                       class="col-md-4 col-form-label text-md-right">Услуга:</label>

                                <div class="col-md-6">
                                    <input id="product" type="text"
                                           class="form-control @error('product') is-invalid @enderror"
                                           name="product" value="{{$products->title}}" required
                                           autocomplete="product" autofocus>

                                    @error('product')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="price_cents"
                                       class="col-md-4 col-form-label text-md-right">Итого грн.:</label>

                                <div class="col-md-6">
                                    <input id="price_cents" type="text"
                                           class="form-control @error('price_cents') is-invalid @enderror"
                                           name="price_cents"
                                           value="{{ number_format( $products->price_cents/100,2,',','')}}"
                                           required autocomplete="price_cents" autofocus>

                                    @error('price_cents')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Оплатить
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
