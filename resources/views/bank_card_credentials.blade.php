@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('payments.enter_card_data') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('checkout')}}">
                            @csrf

                            <input type="hidden" value="{{$client}}" name="client">
                            <input type="hidden" value="{{$product}}" name="product">
                            <input type="hidden" value="{{$product_id}}" name="product_id">
                            <input type="hidden" value="{{$price_cents}}" name="price_cents">

                            <div class="form-group row justify-content-center">

                                <div class="col-md-6">
                                    <input id="card_number" type="text"
                                           class="form-control @error('card_number') is-invalid @enderror"
                                           name="card_number" value="" minlength="16" maxlength="16"
                                           required autofocus placeholder="номер карты">
                                    @error('card_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>




                            <div class="form-group row justify-content-center">

                                <div class="col-md-3">
                                    <input id="mm_yyyy" type="text"
                                           class="form-control @error('mm_yyyy') is-invalid @enderror"
                                           name="mm_yyyy" value="" required autofocus placeholder="мм/гггг"
                                           minlength="7" maxlength="7"
                                    >
                                    @error('mm_yyyy')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-md-3">
                                    <input id="cvv" type="text"
                                           class="form-control @error('cvv') is-invalid @enderror"
                                           name="cvv" value="" required autofocus placeholder="cvv"
                                           minlength="3" maxlength="4">
                                    @error('cvv')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>


                            <div class="form-group row justify-content-center">

                                <div class="col-md-6">
                                    <input id="client" type="text"
                                           class="form-control @error('client') is-invalid @enderror"
                                           name="client" value="{{$client}}" required autofocus
                                           placeholder="ФИО" disabled="disabled">
                                    @error('client')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row justify-content-center">
                                <div class="col-md-6 offset-md-4 ">
                                    <button type="submit" class="btn btn-primary">
                                        Оплатить
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
