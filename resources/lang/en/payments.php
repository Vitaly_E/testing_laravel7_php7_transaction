<?php

return [

    'success' => 'Успешно',
    'no_money' => 'Недостаточно денег',
    'bank_refused_payment' => 'Банк отклонил оплату',
    'enter_client_fio' => 'Введите данные клиента',
    'enter_card_data' => 'Введите даные карты',

];
