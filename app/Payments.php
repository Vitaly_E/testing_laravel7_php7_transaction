<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    public $timestamps = true;
    protected  $hidden = ['created_at','updated_at'];
    protected $fillable = ['sum_total_cents','bank_id','payment_status_id'];
}
